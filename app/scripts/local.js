// Default Invodo afiliate config
'use strict';

window.INVODO_AFF_CONFIG = {
  'test': 'true',
  'rtmpBase': 'rtmp://aoaef.invodo.com/',
  'imageBase': 'http://e.invodo.com/media/',
  'httpBase': 'http://aoael.invodo.com/media/',
  'name': 'Marsha',
  'playbuttoncolor': '',
  'aspectratio': 'WIDESCREEN',
  'defaultquality': '',
  'bufferscreenbranding': 'INVODO',
  'endofcontentbranding': 'INVODO',
  'showcliptitles': 'false',
  'ratingenabled': 'true',
  'autoplaymultipleclips': 'true',
  'playlist': 'false',
  'watermark': 'false',
  'backcolor': '000000',
  'frontcolor': 'FFFFFF',
  'secondarycolor': 'c1267d',
  'share': 'true',
  'og': 'true'
};

// Set the Invodo affiliate
window.INVODO_AFF_CONFIG.affiliate = 'gopro.com';
var IVP_DATA = {
  "video": {
    "autoplay": false,
    "duration": 600,
    "dimensions": {
      "width": 400,
      "height": 600,
      "unit": "px"
    }
  },
  "panels": {
		"enable": true,
		"templates": [],
		"items": []
	},
  "poster": {
    "templates": [
      {
        "id": 1,
        "text": "<div class=\"ivp-gopro-header-container\"> <h2>CAPTURE YOUR SURFING MOMENTS</h2> </div> <div class=\"ivp-gopro-play-box\"> <div class=\"ivp-gopro-play-box-content\"> <p>Watch Video</p><div class=\"ivp-gopro-poster-play\"></div> </div> </div>"
      }
    ],
    "items": [
      {
        "id": 1,
        "enabled": true,
        "template": 1,
        "data": {}
      }
    ]
  },
  "endscreen": {
    "templates": [
      {
        "id": 1,
        "text": "<div class=\"ivp-gopro-header-container\"> <h2>Capture Your Surfing Moments</h2> </div> <div class=\"ivp-gopro-play-box\"> <div class=\"ivp-gopro-play-box-content\"> <img class=\"ivp-gopro-poster-replay\" src=\"images/replay.png\"/><p>Replay Video</p> </div> </div>"
      }
    ],
    "items": [
      {
        "id": 1,
        "enabled": true,
        "template": 1,
        "data": {}
      }
    ]
  },
  "hotspot": {
    "enable": true,
    "templates": [
      {
        "text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"//ixd.invodo.com/client/gopro/images/Floatybackdoor_Thumbnail.jpg\"></div><div class=\"ivp-notification-text\"><h3>${text}</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>",
        "_id": "56058b4cf4546909004b5152",
        "id": 5
      },
      {
        "text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"//ixd.invodo.com/client/gopro/images/Handler_Thumbnail.jpg\"></div><div class=\"ivp-notification-text\"><h3>${text}</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>",
        "_id": "56058b2af4546909004b5151",
        "id": 1
      },
      {
        "text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"//ixd.invodo.com/client/gopro/images/HERO4_Black_Thumbnail.jpg\"></div><div class=\"ivp-notification-text\"><h3>${text}</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>",
        "_id": "56058b1af4546909004b5150",
        "id": 2
      },
      {
        "text": "<span class=\"ivp-hotspot-icon\"></span>",
        "_id": "55f1a905dda55c0900d0eefe",
        "id": 3
      },
      {
        "text": "<div class=\"ivp-notification-inside\"><div class=\"ivp-notification-image\"><img src=\"http://www.reactionface.info/sites/default/files/images/1287666826226.png\"></div><div class=\"ivp-notification-text\"><h3>A Product!</h3></div><div class=\"ivp-notification-learn-more\" role=\"button\"><p>Learn More</p></div></div>",
        "_id": "55f081f625ed2b0900c05eef",
        "id": 4
      }
    ],
    "items": [
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 3
          }
        ],
        "templateId": "56058b4cf4546909004b5152",
        "template": 5,
        "data": {
          "title": "FloatyBackdoor1",
          "text": "Floaty Backdoor"
        },
        "time": {
          "start": 34.2,
          "end": 37.5
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b06f7bcd8cb090051c29d",
        "$$hashKey": "038",
        "id": 0,
        "title": "FloatyBackdoor1",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 2
          }
        ],
        "templateId": "56058b1af4546909004b5150",
        "template": 2,
        "data": {
          "title": "HERO4Black1",
          "text": "HERO4 Black"
        },
        "time": {
          "start": 37.5,
          "end": 41
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b0724bcd8cb090051c2a4",
        "$$hashKey": "039",
        "id": 1,
        "title": "HERO4Black1",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 2
          }
        ],
        "templateId": "56058b1af4546909004b5150",
        "template": 2,
        "data": {
          "title": "HERO4Black2",
          "text": "HERO4 Black"
        },
        "time": {
          "start": 59.75,
          "end": 63.5
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b0724bcd8cb090051c2a4",
        "$$hashKey": "03A",
        "id": 2,
        "title": "HERO4Black2",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 1
          }
        ],
        "templateId": "56058b2af4546909004b5151",
        "template": 1,
        "data": {
          "title": "TheHandler1",
          "text": "The Handler (Floating Hand Grip)"
        },
        "time": {
          "start": 66.5,
          "end": 69.75
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b0758bcd8cb090051c2ab",
        "$$hashKey": "03B",
        "id": 3,
        "title": "TheHandler1",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 3
          }
        ],
        "templateId": "56058b4cf4546909004b5152",
        "template": 5,
        "data": {
          "title": "FloatyBackdoor2",
          "text": "Floaty Backdoor"
        },
        "time": {
          "start": 102.7,
          "end": 106.5
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b06f7bcd8cb090051c29d",
        "$$hashKey": "03C",
        "id": 4,
        "title": "FloatyBackdoor2",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 0
          }
        ],
        "templateId": "55f081f625ed2b0900c05eef",
        "template": 4,
        "data": {
          "title": "EndCardHolder",
          "text": "N/A"
        },
        "time": {
          "start": 600,
          "end": 601
        },
        "notification": true,
        "position": {
          "coords": [
            {
              "x": "",
              "y": ""
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "145px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b0772bcd8cb090051c2b2",
        "$$hashKey": "03D",
        "id": 5,
        "title": "EndCardHolder",
        "type": "notify"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 3
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 3,
        "data": {
          "title": "FloatyBackdoor1Beacon",
          "text": "."
        },
        "time": {
          "start": 34,
          "end": 37.5
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 43.50354694249594,
              "y": 37.383914490976
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b06f7bcd8cb090051c29d",
        "$$hashKey": "03E",
        "id": 6,
        "title": "FloatyBackdoor1Beacon"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 2
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 3,
        "data": {
          "title": "Hero4Black1Beacon",
          "text": "."
        },
        "time": {
          "start": 37.5,
          "end": 41
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 55.90238972773748,
              "y": 11.864502240356455
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b0724bcd8cb090051c2a4",
        "$$hashKey": "03F",
        "id": 7,
        "title": "Hero4Black1Beacon"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 2
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 3,
        "data": {
          "title": "Hero4Black2Beacon",
          "text": "."
        },
        "time": {
          "start": 59.75,
          "end": 62
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 31.890976102722625,
              "y": 22.772159503366794
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b0724bcd8cb090051c2a4",
        "$$hashKey": "03G",
        "id": 8,
        "title": "Hero4Black2Beacon"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 1
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 3,
        "data": {
          "title": "TheHandler1Beacon",
          "text": "."
        },
        "time": {
          "start": 66.5,
          "end": 69.75
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 68.82336622676654,
              "y": 43.34651680893138
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b0758bcd8cb090051c2ab",
        "$$hashKey": "03H",
        "id": 9,
        "title": "TheHandler1Beacon"
      },
      {
        "enabled": true,
        "actions": [
          {
            "id": 0,
            "type": "card",
            "target": 3
          }
        ],
        "templateId": "55f1a905dda55c0900d0eefe",
        "template": 3,
        "data": {
          "title": "FloatyBackdoor2Beacon",
          "text": "."
        },
        "time": {
          "start": 102.75,
          "end": 106
        },
        "notification": false,
        "position": {
          "coords": [
            {
              "x": 52.69230769230769,
              "y": 41.061728704097725
            }
          ],
          "unit": "%"
        },
        "animate": {
          "duration": {
            "start": 0.25,
            "end": 0.25
          },
          "from": {
            "y": "-100%",
            "height": "0",
            "scale": "0"
          },
          "config": [
            {
              "scale": "1",
              "y": "0%",
              "height": "65px"
            },
            {
              "scale": "0",
              "opacity": 0,
              "height": 0
            }
          ]
        },
        "card": "560b06f7bcd8cb090051c29d",
        "$$hashKey": "03I",
        "id": 10,
        "title": "FloatyBackdoor2Beacon"
      }
    ]
  },
  "card": {
    "enabled": true,
    "templates": [
      {
        "text": "<div class=\"ivp-card--large\">\n  <div class=\"ivp-card-content\">\n    <div class=\"ivp-container\">\n      <div class=\"ivp-row\">\n        <div class=\"ivp-col-12 ivp-end-card-text\">\n          <h3>Included in this Bundle</h3>\n        </div>\n        <div class=\"ivp-col-12 ivp-end-card-image\">\n          <img src=\"${image}\">\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"ivp-gopro-card-footer\">\n    <div class=\"ivp-container\">\n      <div class=\"ivp-row\">\n        <div class=\"ivp-col-6\">\n          <h3 class=\"ivp-gopro-bundle-name\">${bundle}</h3>\n          <p class=\"ivp-gopro-card-price\">${price}</p>\n        </div>\n        <div class=\"ivp-col-6\">\n          <div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"#\" target=\"_blank\">Add Bundle To Cart</a></div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>",
        "_id": "560b048abcd8cb090051c29c",
        "id": 2
      },
      {
        "text": "<div class=\"ivp-card--large\">\n  <div class=\"ivp-card-content\">\n    <div class=\"ivp-container\">\n      <div class=\"ivp-row\">\n        <div class=\"ivp-col-8 ivp-card-text\">\n          <h3>${product}</h3>\n          <hr>\n          <p class=\"ivp-gopro-card-description\">${description}</p>\n          <p class=\"ivp-gopro-card-compat\">${compat}</p>\n        </div>\n        <div class=\"ivp-col-4 ivp-card-image\">\n          <img src=\"${image}\">\n        </div>\n      </div>\n    </div>\n  </div>\n  <div class=\"ivp-gopro-card-footer\">\n    <div class=\"ivp-container\">\n      <div class=\"ivp-row\">\n        <div class=\"ivp-col-6\">\n          <h3 class=\"ivp-gopro-bundle-name\">${bundle}</h3>\n          <p class=\"ivp-gopro-card-price\">${price}</p>\n          <div class=\"ivp-gopro-cart-status\">\n            <span class=\"ivp-gopro-cart-icon\"></span> Added to Cart</div>\n        </div>\n        <div class=\"ivp-col-6\">\n          <div class=\"ivp-card-ctas\"><a class=\"ivp-card-cta ivp-card-cta--primary\" href=\"#\" target=\"_blank\">Add Bundle To Cart</a></div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>",
        "_id": "560b0462bcd8cb090051c29b",
        "id": 1
      }
    ],
    "items": [
      {
        "_id": "560b0772bcd8cb090051c2b2",
        "title": "GoPro End",
        "template": 2,
        "data": {
          "image": "//ixd.invodo.com/client/gopro/images/endscreen.png",
          "bundle": "SURFING BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 0,
        "position": {
          "x": '-50%',
          "y": '6%'
        }
      },
      {
        "_id": "560b0758bcd8cb090051c2ab",
        "title": "GoPro_Handler",
        "template": 1,
        "data": {
          "product": "The Handler (Floating Hand Grip)",
          "description": "Floating GoPro hand grip for stable shooting in and out of the water.",
          "compat": "Compatability: All Go Pro Cameras",
          "image": "//ixd.invodo.com/client/gopro/images/Handler_Thumbnail.jpg",
          "bundle": "SURFING BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 1,
        "position": {
          "x": '-50%',
          "y": '6%'
        }
      },
      {
        "_id": "560b0724bcd8cb090051c2a4",
        "title": "GoPro_Hero4Black",
        "template": 1,
        "data": {
          "product": "HERO4 Black",
          "description": "Pro-quality capture. Simply the best.",
          "compat": "Compatability: All Go Pro Cameras",
          "image": "//ixd.invodo.com/client/gopro/images/HERO4_Black_Thumbnail.jpg",
          "bundle": "SURFING BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 2,
        "position": {
          "x": '-50%',
          "y": '6%'
        }
      },
      {
        "_id": "560b06f7bcd8cb090051c29d",
        "title": "GoPro_FloatyBackdoor",
        "template": 1,
        "data": {
          "product": "Floaty Backdoor",
          "description": "This easy-to-spot flotation keeps your GoPro afloat.",
          "compat": "Compatability: All Go Pro Cameras",
          "image": "//ixd.invodo.com/client/gopro/images/Floatybackdoor_Thumbnail.jpg",
          "bundle": "SURFING BUNDLE",
          "price": "US$499.99"
        },
        "size": "medium",
        "animate": {
          "duration": {
            "start": 0.3,
            "end": 0.3
          },
          "from": {
            "left": "50%",
            "opacity": 0
          },
          "config": [
            {
              "y": "30%",
              "opacity": 1
            },
            {
              "opacity": 0,
              "y": "7%"
            }
          ]
        },
        "id": 3,
        "position": {
          "x": '-50%',
          "y": '6%'
        }
      }
    ]
  }
}
